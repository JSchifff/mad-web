var express = require("express");
var bodyParser = require("body-parser");
const { Client } = require('pg');
const { parse } = require('querystring');
const SqlString = require('sqlstring');
const formidable = require('formidable');
var fs = require('fs');
const knox = require('knox');
var busboy = require('connect-busboy');

const aws = require('aws-sdk'),
    multer = require('multer'),
    multerS3 = require('multer-s3');

aws.config.update({
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  region: 'us-east-1'
});

var s3 = new aws.S3();
var app = express();

var upload = multer({
  storage: multerS3({
      s3: s3,
      bucket: 'mad-menu-pics',
      key: function (req, file, cb) {
          console.log(file);
          cb(null, file.originalname); //use Date.now() for unique file keys
      }
  })
});

app.use('/api', bodyParser.json());
app.use('/specialapi', busboy()); 
app.engine('js', require('ejs').renderFile);
//app.use(express.static(__dirname + '../public'));

const client = new Client({
  connectionString: process.env.DATABASE_URL,
  ssl: true,
});

const tables = {
  "Categories": "menu_cat",
  "Items": "menu_item",
  "Meals": "menu_meal",
  "Orders": "orders",
  "OrderMeals": "order_meals"
};

/*
..######..########....###....########..########.....######..########.########..##.....##.########.########.
.##....##....##......##.##...##.....##....##.......##....##.##.......##.....##.##.....##.##.......##.....##
.##..........##.....##...##..##.....##....##.......##.......##.......##.....##.##.....##.##.......##.....##
..######.....##....##.....##.########.....##........######..######...########..##.....##.######...########.
.......##....##....#########.##...##......##.............##.##.......##...##....##...##..##.......##...##..
.##....##....##....##.....##.##....##.....##.......##....##.##.......##....##....##.##...##.......##....##.
..######.....##....##.....##.##.....##....##........######..########.##.....##....###....########.##.....##
*/



var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    client.connect();
      
    console.log("App now running on " + port);
});



/*
..######...########.########..######.
.##....##..##..........##....##....##
.##........##..........##....##......
.##...####.######......##.....######.
.##....##..##..........##..........##
.##....##..##..........##....##....##
..######...########....##.....######.
*/
app.get("/api/menuCategories", function(req, res) {
  client.query(`SELECT * FROM menu_cats;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/menuItems", function(req, res) {
  client.query(`SELECT * FROM menu_item;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/menuItems/:catID", function(req, res) {
  console.log(req.params);
  client.query(`SELECT * FROM menu_cats LEFT JOIN menu_item USING(cat_id) WHERE cat_id = ` + SqlString.escape(req.params.catID) + `;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.setHeader('Access-Control-Allow-Origin', '*');

    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/menuMeals", function(req, res) {
  client.query(`SELECT * FROM menu_meal;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/menuMeals/:itemID", function(req, res) {
  client.query(`SELECT * FROM menu_item LEFT JOIN menu_meal USING(item_id) WHERE item_id = ` + SqlString.escape(req.params.itemID) + `;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/allMenuItems", function(req, res) {
  client.query(`
  SELECT 
    mc.cat_id,
    name,
    items
  from menu_cats mc
  LEFT JOIN (
    select cat_id,
    json_agg(
      json_build_object(
        'imagePath', mi.imagePath,
        'itemName', mi.itemName,
        'itemDesc', mi.itemDesc,
        'glutenFree', mi.glutenFree,
        'nutFree', mi.nutFree,
        'lactoseFree', mi.lactoseFree,
        'vegFriendly', mi.vegFriendly,
        'available', mi.available,
        'Meals', mm
      )
    ) items
    FROM menu_item mi

    LEFT JOIN (
      SELECT item_id,
      json_agg(mm.*) meals
      FROM menu_meal mm
      GROUP BY 1
    )
    mm ON mi.item_id = mm.item_id
    GROUP BY cat_id
  ) mi ON mc.cat_id = mi.cat_id;
  `, (err, queryRes) => {
  // client.query('SELECT * FROM menu_cats LEFT JOIN menu_item USING(cat_id);', (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.status(200).json(queryRes.rows);
  });
});


/*
.########...#######...######..########..######.
.##.....##.##.....##.##....##....##....##....##
.##.....##.##.....##.##..........##....##......
.########..##.....##..######.....##.....######.
.##........##.....##.......##....##..........##
.##........##.....##.##....##....##....##....##
.##.........#######...######.....##.....######.
*/
//Function that pulls needed attributes from request body, passes the relevant data to the callback
//The callback inserts the data into the database
function getRequestData(req, callback) {
  var data;
  let body = '';
    req.on('data', chunk => {
        body += chunk.toString(); // convert Buffer to string
    });
    req.on('end', () => {
        data = parse(body);
        callback(data);
    }); 
}


app.post("/specialapi/menuItemsImg", upload.single('image'), function(req, res) {
  res.send("done");
});

app.post("/api/menuItems/:catID", function(req, res) {
//   var form = new formidable.IncomingForm();
//   form.parse(req, function (err, fields, files) {
//     console.log(files);
//     var oldpath = files.image.path;
//     var newpath = __dirname + 'images' + files.image.name;
//     fs.copyFile(oldpath, newpath, function (err) {
//       if (err) throw err;
//       console.log("Item pic uploaded");

//     });
// });
  

  getRequestData(req, postMenuItem);
        
  function postMenuItem(data) {
    console.log("Posting to database");
    var requiredProperties = [
      "name",
      "desc",
      "catID"
    ];
    if(requiredProperties.every(prop => prop in data)) {
      var falseChecks = ['glutenFree', 'nutFree', 'lactoseFree', 'available', 'fname'];
      falseChecks.forEach(prop => {
        if(data[prop] == null) { data[prop] = false; }
      });

      console.log("trying SQL:   " + data.name);
      console.log(data);

      var query = `INSERT INTO menu_item(itemname, itemdesc, glutenfree, nutfree, lactosefree, vegfriendly, available, cat_id, imagepath)
       VALUES(` + 
        SqlString.escape(data.name) + `,` + 
        SqlString.escape(data.desc) + `,` + 
        SqlString.escape(data.glutenFree) + `,` + 
        SqlString.escape(data.nutFree) + `,` + 
        SqlString.escape(data.lactoseFree) + `,` + 
        SqlString.escape(data.vegFriendly) + `,` + 
        SqlString.escape(data.available) + `,` + 
        SqlString.escape(data.catID) + `,`
        + SqlString.escape('https://s3.us-east-2.amazonaws.com/mad-menu-pics/' + data.fname) + `);`;
      console.log(data.name);
      client.query(query, (err, res) => {
        if (err) {
          console.log(err.stack);

        } else {
          console.log(res.rows[0]);
        }
      });

      //client.query('INSERT INTO menu_cats(name) VALUES(' + SqlString.escape(data.name) + ');');
    }
  }
});

app.post("/api/menuCategories", function(req, res) {
  getRequestData(req, postMenuCat);
        
  function postMenuCat(data) {
    console.log(data);
    if('name' in data) {
      // console.log("trying SQL:   " + data.name);

      // var query = 'INSERT INTO menu_cats(name) VALUES(\'' + SqlString.escape(body.name) + '\');';
      console.log("Adding " + SqlString.escape(data.name) + " to MenuCategories");

      client.query('INSERT INTO menu_cats(name) VALUES(' + SqlString.escape(data.name) + ');');
      res.json('complete: true');
    }
  }
});

app.post("/api/menuMeals/:itemID", function(req, res) {
  getRequestData(req, postMenuMeal);
  function postMenuMeal(data) {
    client.query(`
    INSERT INTO menu_meal(price, mealsize, item_id)
    VALUES(` + SqlString.escape(data.price) + `, ` + SqlString.escape(data.mealSize) + `, `+ SqlString.escape(data.itemID) + `)`);
    res.json("completed: true")
  }
});


/*
.########..########.##.......########.########.########..######.
.##.....##.##.......##.......##..........##....##.......##....##
.##.....##.##.......##.......##..........##....##.......##......
.##.....##.######...##.......######......##....######....######.
.##.....##.##.......##.......##..........##....##.............##
.##.....##.##.......##.......##..........##....##.......##....##
.########..########.########.########....##....########..######.
*/
app.get("/api/delete/categories/:catID", function(req, res) {

  client.query("DELETE FROM menu_cats WHERE cat_id = " + SqlString.escape(req.params.catID) + ";")
  .then(res.json("completed: true"));
});

app.get("/api/delete/menuItems/:itemID", function(req, res) {

  client.query("DELETE FROM menu_item WHERE item_id = " + SqlString.escape(req.params.itemID) + ";")
  .then(res.json("completed: true"));
});

app.get("/api/delete/menuMeals/:mealID", function(req, res) {

  client.query("DELETE FROM menu_meal WHERE meal_id = " + SqlString.escape(req.params.mealID) + ";")
  .then(res.json("completed: true"));
});


/*
.##.....##.####.########.##......##..######.
.##.....##..##..##.......##..##..##.##....##
.##.....##..##..##.......##..##..##.##......
.##.....##..##..######...##..##..##..######.
..##...##...##..##.......##..##..##.......##
...##.##....##..##.......##..##..##.##....##
....###....####.########..###..###...######.
*/
  //Index Page
  app.get("/", function (req, res) {
    
    res.status(200).render("index.js", {root: __dirname});
    
  });

  app.get("/categories", function (req, res) {
    
    res.status(200).render("categories.js", {root: __dirname});
    
  });

  app.get("/categories/:catID", function (req, res) {
    
    res.status(200).render("categoryItems.js", {root: __dirname});
    
  });

  app.get("/categoriesImg/:catID", function (req, res) {
    
    res.status(200).render("categoryItemsImg.js", {root: __dirname});
    
  });

  app.get("/items/:itemID", function (req, res) {
    
    res.status(200).render("itemMeals.js", {root: __dirname});
    
  });
