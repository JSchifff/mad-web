
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
        </script>
    </head>

    <body>
        <h1>API Endpoints</h1>
        <h3>GET</h3>
        <ul>
            <li><a href="/api/menuCategories">/api/menuCategories == All Menu Categories</a></li>
            <li><a href="/api/menuItems/8">/api/menuItems/:cat_id == Details of a particular category</a></li>
            <li><a href="/api/menuItems">/api/menuItems == Details of all items in database</a></li>
            <li><a href="/api/menuMeals">/api/menuMeals == Details of all meals in database(currently empty)</a></li>
            <li><a href="/api/allMenuItems">/api/allMenuItems == Details of all food data in database as massive JSON object</a></li>

        </ul>

        <a href="/categories"><h3>VIEW MENU</h3></a>

    </body>
</html>