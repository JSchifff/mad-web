<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>

        function getMeals(item_id) {
            $.get("https://gentle-atoll-23251.herokuapp.com/api/menuMeals/" + item_id, function(data, status) {
                console.log(data);
                $("#title").text(data[0].itemname);
                $("#meals").html('');
                for(res of data) {
                    
                    $("#meals").append("<strong>" + res.mealsize + "</strong>: $" + res.price
                    + `<button value="DELETE" onCLick="removeMeal(` + res.meal_id + `, ` + item_id + `)">DELETE</button>`
                    + "<br />");
                }
            
            });
        };

        function removeMeal(meal_id, item_id) {
            $.get("https://gentle-atoll-23251.herokuapp.com/api/delete/menuMeals/" + meal_id)
            .then(getMeals(item_id)); 
        }  

        $(document).ready(function () {
            
            const item_id = window.location.href.split("/").pop();
            //$("#itemForm").attr("action", "../api/menuItems/" + cat_id);
            $("#itemID").attr("value", item_id);



            $("#submit").click(function() {                
                $.post("https://gentle-atoll-23251.herokuapp.com/api/menuMeals/" + item_id,
                    $("#itemForm").serialize()
                ).then(getMeals(item_id));
                
            });

            //console.log(cat_id);
            getMeals(item_id);
            
        });

        
        </script>
    </head>

        <body>
        <div>
            <form id="itemForm" method="post" enctype="multipart/form-data">
                <input type="hidden" name="itemID" id="itemID" />
                Size: <input type="text" id="name" name="mealSize" required /><br />
                Price: <input type="number" name="price" required step="0.01" />
                <input type="button" id="submit" value="Add Item!" />
            </form>

        </div>
        <hr />
        <h1 id="title"></h1>
        <div id="meals">
            
        </div>
    </body>
</html>