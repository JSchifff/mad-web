<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            function getCats() {
                $.get("https://gentle-atoll-23251.herokuapp.com/api/menuCategories", function(data, status) {
                    console.log(data);
                    $("#results").html("");
                    for(let res of data)
                    {
                        $("#results").append(`
                        <li>
                            <a href='/categories/` + res.cat_id + `'>` + res.name + `</a>
                            <button value="DELETE" onCLick="removeCat(` + res.cat_id + `)">DELETE</button>
                        </li>`);
                    }
                });
            };

            function removeCat(cat_id) {
                $.get("https://gentle-atoll-23251.herokuapp.com/api/delete/categories/" + cat_id)
                .then(getCats()); 
            }   


            $(document).ready(function () {




                getCats();

                $("#postButton").click(function() {
                    //console.log("khrg");
                    var name = $("#nameField").val();
                    console.log(name);
                    if(name.length >= 1) {
                        $.post("https://gentle-atoll-23251.herokuapp.com/api/menuCategories",
                            "name=" + name
                        ).then(getCats());
                    }
                });

                
            });


        </script>
    </head>
    <body>
        <div id="form">
            <input type="text" id="nameField" />
            <input type="button" id="postButton" value="Add Category!" />
        </div>
        <hr />
        <div>
            <ul id="results"></ul>
        </div>
    </body>
</html>