<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>

        function getItems(cat_id) {
            $.get("https://gentle-atoll-23251.herokuapp.com/api/menuItems/" + cat_id, function(data, status) {
                console.log(data);
                $("#title").text(data[0].name);
                $("#category").html('');
                for(res of data) {
                    
                    $("#category").append(`<div class='item'><a href=\"/items/` + res.item_id + `\"><h3 style="display: inline;">` + res.itemname + `</h3></a>` 
                    + res.itemdesc
                    + `<button value="DELETE" onCLick="removeItem(` + res.item_id + `, ` + cat_id + `)">DELETE</button>`

                    + "<br />"
                    + "<ul>"
                    + "<li>Available: " + res.available + "</li>"
                    + "<li>Gluten Free: " + res.glutenfree + "</li>"
                    + "<li>Nut Free: : " + res.nutfree + "</li>"
                    + "<li>Lactose Free: : " + res.lactosefree + "</li>"
                    + "<li>Vegetarian : " + res.vegfriendly + "</li>"
                    + "</ul>" 
                    + `</div>`);
                }
            
            });
        };

        function removeItem(item_id, cat_id) {
            console.log(item_id);
            $.get("https://gentle-atoll-23251.herokuapp.com/api/delete/menuItems/" + item_id)
            .then(getItems(cat_id)); 
        }  

        $(document).ready(function () {
            
            const cat_id = window.location.href.split("/").pop();
            $("#itemForm").attr("action", "../api/menuItems/" + cat_id);
            $("#catID").attr("value", cat_id);



            console.log(cat_id);
            getItems(cat_id);
            
        });

        
        </script>
    </head>
    <body>
        <div>
            <form id="itemForm" method="post" enctype="multipart/form-data">
                Name: <input type="text" id="name" name="name" required /><br />
                <input type="hidden" name="catID" id="catID" />
                Description: <input type="text" id="desc" name="desc" required /><br />
                Image: <input type="file" id="img"  name="image" /><br />
                Nut Free: <input type="checkbox" id="nutFree" name="nutFree" /><br />
                Gluten Free: <input type="checkbox" id="glutenFree" name="glutenFree" /><br />
                Lactose Free: <input type="checkbox" id="lactoseFree" name="lactoseFree" /><br />
                Veg friendly?: No-><input type="radio" name="vegFriendly" value="no" checked />
                    Vegetarian-><input type="radio" name="vegFriendly" value="vegetarian"/>
                    Vegan-><input type="radio" name="vegFriendly" value="vegan" /><br />
                Available: <input type="checkbox" id="available" name="available" checked /><br />
                <input type="button" id="submit" value="Add Item!" />
            </form>

        </div>
        <hr />
        <h1 id="title"></h1>
        <div id="category">
            
        </div>
    </body>
</html>